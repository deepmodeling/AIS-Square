This is a v3.0.0rc0 compatible version of [DPA-2.3.0-v3.0.0beta4](https://www.aissquare.com/models/detail?pageType=models&name=DPA-2.3.0-v3.0.0b4&id=279) model, in which all metrics remain consistent. Please ensure the usage of the correct code version (**v3.0.0rc0**) corresponding to this model.

First of all, we are excited to announce the **rc0 version of DeePMD-kit v3**. DeePMD-kit v3 allows you to train and run deep potential models on top of TensorFlow or PyTorch. DeePMD-kit v3 also supports the [DPA-2 model](https://arxiv.org/abs/2312.15492), a novel architecture for large atomic models.

# Highlight

In v3.0.0rc0 verion, the DPA2 model structure has been optimized, supporting three model configurations: **small**, **medium**, and **large**. This allows users to select the appropriate configuration based on different scenarios, Detailed configurations can be found [here](https://github.com/deepmodeling/deepmd-kit/tree/v3.0.0rc0/examples/water/dpa2).

Notably, the medium DPA2 model has better accuracy than the DPA2 model in beta3 while **doubling overall training and inference efficiency**.

For pretrained models, we compared the inference efficiency and accuracy on [`2.3.1-rc0-medium`](https://www.aissquare.com/models/detail?pageType=models&name=DPA-2.3.1-v3.0.0rc0&id=287) and [`2.2.0-b3`](https://www.aissquare.com/models/detail?pageType=models&name=DPA-2.2.0-v3.0.0b3&id=272):

## Efficiency comparison


| Pretrained Model                      | [2.3.1-rc0-medium](https://www.aissquare.com/models/detail?pageType=models&name=DPA-2.3.1-v3.0.0rc0&id=287) | [2.2.0-b3](https://www.aissquare.com/models/detail?pageType=models&name=DPA-2.2.0-v3.0.0b3&id=272) |
| ------------------------------------- | ----------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------- |
| Python inference speed  (s/100 times) | **3.7**                                                                                                     | 6.3                                                                                                |

* Python inference speed is measured on water systems with 192 atoms.
* This test is performed on single A800 GPU card with 40G memory.

## Accuracy comparison

Detailed test RMSEs of pretrained models on several baseline systems:

(energy: ev/atom, force: eV/Å)


| Datasets         | Weight | [2.3.1-rc0-medium](https://www.aissquare.com/models/detail?pageType=models&name=DPA-2.3.1-v3.0.0rc0&id=287) E | [2.2.0-b3](https://www.aissquare.com/models/detail?pageType=models&name=DPA-2.2.0-v3.0.0b3&id=272) E | [2.3.1-rc0-medium](https://www.aissquare.com/models/detail?pageType=models&name=DPA-2.3.1-v3.0.0rc0&id=287) F | [2.2.0-b3](https://www.aissquare.com/models/detail?pageType=models&name=DPA-2.2.0-v3.0.0b3&id=272) F |
| ---------------- | ------ | ------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------- |
| MPtraj           | 2.0    | 1.14E-01                                                                                                      | **Not Supported**                                                                                    | 2.26E-01                                                                                                      | **Not Supported**                                                                                    |
| Domains_Alloy    | 2.0    | 2.17E-02                                                                                                      | 2.01E-02                                                                                             | 1.51E-01                                                                                                      | 1.41E-01                                                                                             |
| Domains_Anode    | 1.0    | 1.33E-03                                                                                                      | 1.94E-03                                                                                             | 5.19E-02                                                                                                      | 4.36E-02                                                                                             |
| Domains_Cluster  | 1.0    | 4.80E-02                                                                                                      | 4.75E-02                                                                                             | 1.46E-01                                                                                                      | 1.40E-01                                                                                             |
| Domains_Drug     | 2.0    | 1.14E-02                                                                                                      | 1.23E-02                                                                                             | 1.46E-01                                                                                                      | 1.18E-01                                                                                             |
| Domains_FerroEle | 1.0    | 1.74E-03                                                                                                      | 1.64E-03                                                                                             | 5.42E-02                                                                                                      | 4.54E-02                                                                                             |
| Domains_OC2M     | 2.0    | 2.89E-02                                                                                                      | 2.39E-02                                                                                             | 1.76E-01                                                                                                      | 1.59E-01                                                                                             |
| Domains_SSE-PBE  | 1.0    | 2.02E-03                                                                                                      | 1.90E-03                                                                                             | 7.64E-02                                                                                                      | 6.51E-02                                                                                             |
| Domains_SemiCond | 1.0    | 8.26E-03                                                                                                      | 5.70E-03                                                                                             | 1.48E-01                                                                                                      | 1.33E-01                                                                                             |
| H2O_H2O-PD       | 1.0    | 1.24E-03                                                                                                      | 8.32E-04                                                                                             | 5.16E-02                                                                                                      | 3.49E-02                                                                                             |
| Metals_AgAu-PBE  | 0.2    | 3.99E-03                                                                                                      | 6.80E-03                                                                                             | 2.86E-02                                                                                                      | 3.05E-02                                                                                             |
| Metals_AlMgCu    | 0.3    | 1.15E-02                                                                                                      | 2.16E-02                                                                                             | 3.10E-02                                                                                                      | 2.58E-02                                                                                             |
| Metals_Cu        | 0.1    | 3.35E-03                                                                                                      | 2.56E-03                                                                                             | 2.16E-02                                                                                                      | 1.90E-02                                                                                             |
| Metals_Sn        | 0.1    | 1.68E-02                                                                                                      | 1.17E-02                                                                                             | 7.66E-02                                                                                                      | 7.25E-02                                                                                             |
| Metals_Ti        | 0.1    | 1.99E-02                                                                                                      | 1.11E-02                                                                                             | 1.10E-01                                                                                                      | 1.07E-01                                                                                             |
| Metals_V         | 0.1    | 9.74E-03                                                                                                      | 7.57E-03                                                                                             | 1.16E-01                                                                                                      | 1.09E-01                                                                                             |
| Metals_W         | 0.1    | 2.74E-02                                                                                                      | 1.70E-02                                                                                             | 1.72E-01                                                                                                      | 1.65E-01                                                                                             |
| Others_C12H26    | 0.1    | 6.91E-02                                                                                                      | 5.53E-02                                                                                             | 7.34E-01                                                                                                      | 5.46E-01                                                                                             |
| Others_HfO2      | 0.1    | 2.76E-03                                                                                                      | 2.12E-03                                                                                             | 1.24E-01                                                                                                      | 1.04E-01                                                                                             |

This table demonstrates that this DPA2 medium model has **comparable accuracy to the older version of DPA2, but is twice as efficient**.

**Additionally, the inclusion of the MPtraj dataset enables users to apply the model across a broad spectrum of applications with large configuration and composition spaces.**

**Note**: Compared to 2.2.0-b3, the 2.3.1-rc0-medium model features a reduced number of parameters in its descriptors and incorporates the new MPtraj dataset. While the weighted average RMSE has marginally increased in this baseline, the precision across most datasets remains consistent, with some instances demonstrating improved performance.

It is worth noting that the weighted logarithmic mean is used to calculate the Weighted Error for each column, rather than the weighted arithmetic mean reported in the [DPA-2](https://arxiv.org/abs/2312.15492) paper.

We provide DPA2 medium model pretrained on 28 datasets (**PyTorch backend**), which is compatible with [DeePMD-kit v3 rc0 version](https://github.com/deepmodeling/deepmd-kit/releases/tag/v3.0.0rc0).
The corresponding small and large pretrained models will be released soon.

In this page, we systematically report the **usage method of the pretrained model**, **the evaluation of the model** and **the data for pretraining**.

If you have any problems, ideas or suggestions about OpenLAM, welcome to have a discussion in this page or [GitHub Discussions](https://github.com/deepmodeling/deepmd-kit/discussions).

# How to use

## Installation

To use the pretrained model, you need to first install the corresponding version of DeePMD-kit.
You can try easy installation:

```bash
pip install torch torchvision torchaudio
pip install git+https://github.com/deepmodeling/deepmd-kit@v3.0.0rc0
```

For other installation options, please visit the [Releases page](https://github.com/deepmodeling/deepmd-kit/releases/tag/v3.0.0rc0) page to download the off-line package for **deepmd-kit v3.0.0rc0**, and refer to the [official documentation](https://docs.deepmodeling.com/projects/deepmd/en/v3.0.0rc0/install/easy-install.html) for off-line installation instructions.

## Use the pretrained model

The pretrained model can be used directly for prediction tasks, such as serving as an ASE calculator.

This model is pretrained in a multi-task manner, featuring a unified backbone (referred to as the unified descriptor) and several fitting nets for different datasets.
For detailed information, refer to the [DPA-2 paper](https://arxiv.org/abs/2312.15492).

The first step involves selecting a specific fitting net from the model to make predictions.
To list the available fitting nets (`model-branch`) in this pretrained model, use the following command:

```bash
dp --pt show DPA2_medium_28_10M_rc0.pt model-branch
```

This will generate an output similar to the following:

```bash
Available model branches are 
['Domains_Alloy', 'Domains_Anode', 'Domains_Cluster', 'Domains_Drug', 'Domains_FerroEle', 'Domains_OC2M', 'Domains_SSE-PBE', 'Domains_SemiCond', 
'H2O_H2O-PD', 'Metals_AgAu-PBE', 'Metals_AlMgCu', 'Metals_Cu', 'Metals_Sn', 'Metals_Ti', 'Metals_V', 'Metals_W', 'Others_C12H26', 'Others_HfO2', 'Domains_ANI', 
'Domains_SSE-PBESol', 'Domains_Transition1x', 'H2O_H2O-DPLR', 'H2O_H2O-PBE0TS-MD', 'H2O_H2O-PBE0TS', 'H2O_H2O-SCAN0', 'Metals_AgAu-PBED3', 'Others_In2Se3', 
'MP_traj_v024_alldata_mixu', 'RANDOM'], where 'RANDOM' means using a randomly initialized fitting net.
```

Select a fitting net that closely matches your system.
Ensure that the elements in your system are included in the corresponding fitting net if you are conducting direct predictions or simulations.
For more information on the pretrained datasets, refer to [below](#data-used-for-pretraining).

Assuming you choose `H2O_H2O-PD`, you can first freeze the model branch from the multi-task pretrained model:

```bash
dp --pt freeze -c DPA2_medium_28_10M_rc0.pt -o frozen_model.pth --head H2O_H2O-PD
```

Then you can use the following Python code for prediction or optimization:

```python
## Compute potential energy
from ase import Atoms
from deepmd.calculator import DP as DPCalculator
dp = DPCalculator("frozen_model.pth")
water = Atoms('H2O', positions=[(0.7601, 1.9270, 1), (1.9575, 1, 1), (1., 1., 1.)], cell=[100, 100, 100])
water.calc = dp
print(water.get_potential_energy())
print(water.get_forces())

## Run BFGS structure optimization
from ase.optimize import BFGS
dyn = BFGS(water)
dyn.run(fmax=1e-6)
print(water.get_positions())
```

## Train a model

First, you need to prepare your training data in deepmd format, see [here](https://github.com/deepmodeling/deepmd-kit/blob/v3.0.0rc0/doc/data/system.md) for details.
You can also use [dpdata](https://github.com/deepmodeling/dpdata) tools to convert from other data formats (such as `vasp/outcar`, `cp2k/output`, `ase/structure`, etc.).

### Train from scratch

This is the standard way to train a model from scratch. You can refer to the official tutorial in the DeePMD-kit repository [here](https://docs.deepmodeling.com/projects/deepmd/en/v3.0.0rc0/train/training.html).
In simple terms, you need to replace the `model/type_map` and `training/training(validation)_data` sections in the example training inputs (`input_torch_medium.json` in this page) with those corresponding to your dataset, and then proceed with the training process:

```bash
dp --pt train input_torch_medium.json
```

We recommend that you initially try training various models from scratch on your system. This will give you a more intuitive understanding of the performance of the pretrained models before proceeding with the fine-tuning process described below.

### Finetune the pretrained model

To fine-tune a pretrained model, you need to ensure that the model configuration (`model/descriptor` and `model/fitting_net`) matches exactly with the pretrained model.
The `input_torch_medium.json` file provided on this page contains the same model configuration as the pretrained model.

To prepare for fine-tuning, replace the `model/type_map` and `training/training(validation)_data` sections in `input_torch_medium.json` with your specific dataset information, just the same as from scratch.
Additionally, adjust the learning rate for fine-tuning, typically a small learning rate such as 1e-4 for `learning_rate/start_lr` is recommended.

While selecting a fitting net as mentioned [above](#use-the-pretrained-model), note that for fine-tuning, the specific choice of fitting net does not significantly impact the process.

Execute the following command to fine-tune the pretrained model on your dataset (e.g., `Domains_Alloy`):

```bash
dp --pt train input_torch_medium.json --finetune DPA2_medium_28_10M_rc0.pt --model-branch Domains_Alloy
```

### (Advanced) Multi-task finetune

For advanced users, you can fine-tune the model with multiple datasets simultaneously. For more detailed instructions, refer to the [multi-task fine-tuning documentation](https://github.com/deepmodeling/deepmd-kit/blob/v3.0.0rc0/doc/train/finetuning.md#multi-task-fine-tuning).
The multi-task pretraining script is also provided as `pretrain_input.json` in this page.

## Zero-shot

Zero-shot learning is a machine learning scenario in which an AI model is trained to recognize and categorize objects or concepts without having seen any examples of those categories or concepts beforehand.

In the context of large atomic models, zero-shot can be considered as a test of model's generalization. For example, we can compare it with the standard deviation of the orginal data. If the zero-shot RMSE is smaller than the corresponding standard deviation, the model shows the ability of zero-shot generalization.

Furthermore, if we want to know whether the pretrained model is suitable for a new circumstance, or which head should be selected for fine-tuning, we can also perform the zero-shot test.

An instruction for zeroshot can be found at [https://bohrium.dp.tech/notebooks/57552161357](https://bohrium.dp.tech/notebooks/57552161357) via Bohrium Notebook which is a cloud-native computing platform.

To be specific, given a new downstream system, we use `dp --pt change-bias` to do zero-shot procedure, which will inherit the neural network parameters of descriptor in pretrained multitask model. The fitting net can either reinit or inherit the fitting net from any branch of the pre-trained model depending on the argument *--model-branch*.

```bash
dp --pt change-bias model.pt -s <your_system> --model-branch Domains_Alloy
```

to determine the energy bias of the downstream task, instead of those used in the pretraining stage. Then we can directly test the zero-shot of the model by selecting the head via `dp --pt test`.

We also use GST_GAP_22 as a practical example. The main training dataset for GST_GAP_22,is calculated using the PBEsol functional. GST_GAP_22 contains configurations of phase-change materials on the quasi-binary GeTe-Sb2Te3 (GST) line of chemical compositions. Data was used for training a machine learning interatomic potential to simulate a range of germanium-antimony-tellurium compositions under realistic device conditions. More details can be refered to [https://materials.colabfit.org/id/DS_r3hav37ufnmb_0](https://materials.colabfit.org/id/DS_r3hav37ufnmb_0). Notice that in the SemiCond branch of the pretrained model, the set of data contains structure-to-energy-force labels for 20 semiconductors, namely, Si, Ge, SiC, BAs, BN, AlN, AlP, AlAs, InP, InAs, InSb, GaN, GaP, GaAs, CdTe, InTe-In2Te3, CdSe-CdSe2, InSe-In2Se3, ZnS, CdS-CdS2. We first test the 28heads-10m-model's zero-genralization.


| Model Branch         | Energy RMSE (eV/atom) | Force RMSE (eV/Å) |
| -------------------- | --------------------- | ------------------- |
| Domains_Alloy        | 4.13E-01              | 7.44E-01            |
| Domains_Anode        | 6.71E-01              | 1.04E+00            |
| Domains_Cluster      | 1.03E+00              | 1.71E+00            |
| Domains_Drug         | 6.67E-01              | 7.86E-01            |
| Domains_FerroEle     | 9.63E-01              | 1.52E+00            |
| **Domains_OC2M**     | **3.71E-01**          | **3.63E-01**        |
| Domains_SSE-PBE      | 5.90E-01              | 7.52E-01            |
| **Domains_SemiCond** | **4.08E-01**          | **3.53E-01**        |
| H2O_H2O-PD           | 7.29E-01              | 6.52E-01            |
| Metals_AgAu-PBE      | 7.89E-01              | 1.03E+00            |
| Metals_AlMgCu        | 5.93E-01              | 1.16E+00            |
| Metals_Cu            | 5.97E-01              | 1.07E+00            |
| Metals_Sn            | 5.50E-01              | 5.10E-01            |
| Metals_Ti            | 6.24E-01              | 1.07E+00            |
| Metals_V             | 8.64E-01              | 1.06E+00            |
| Metals_W             | 1.03E+00              | 1.49E+00            |
| Others_C12H26        | 9.38E-01              | 1.21E+00            |
| Others_HfO2          | 8.49E-01              | 9.97E-01            |

The energy and force std of this system is 0.96 eV/atom, and 0.73 eV/Å. For 28heads-10m model, the SemiCond branch's zero-shot RMSE for energy and force is 0.41 eV/atom. and 0.35 eV/Å, which indicate a good zero-shot generalization of the pretrained-model. Besides, this test also demonstrates that the selection of model branch in zero-shot has a large influence on the performance. Meanwhile, OC2M head also performs well in GeTe-Sb2Te3(GST) system.

# Data used for pretraining

## Alloy

**model-branch** : `Domains_Alloy`

* This dataset is generated using the DP-GEN scheme and comprises structure-energy-force-virial data for 53 typical metallic elements (Li, Be, Na, Mg, Al, Si, K, Ca, Sc, Ti, V, Cr, Mn,Fe,Co,Ni,Cu,Zn,Ga,Ge,Sr,Y,Zr,Nb,Mo,Ru,Rh,Pd,Ag,Cd,In,Sn,La,Hf,Ta,W, Re, Os, Ir, Pt, Au, Pb, Ce, Pr, Nd, Sm, Gd, Tb, Dy, Ho, Er, Tm, Lu) The dataset encompasses a diverse array of crystal configurations, featuring FCC, BCC, and HCP structures, as well as intermetallic compounds and amorphous structures with stochastic vacancies. The dataset contains three categories, random substitutional solid solutions, elementary substances, and intermetallic compounds. All density functional theory (DFT) calculations were conducted using the ABACUS package. The exchange-correlation functional was described by the generalized gradient approximation (GGA) in the Perdew-Burke-Ernzerhof (PBE) form. Norm-conserving pseudopotentials were adopted. The cutoff energy of the plane wave basis was set to be 100 Rydberg, and the Monkhorst-Pack k-point mesh was chosen with a reciprocal space resolution of 0.05 Bohr−1. The self-consistent field iteration stops when the difference in total electron density of consecutive iterations is less than 1e−6 eV.

More details and data access can be refered to [Alloy-data](https://www.aissquare.com/datasets/detail?pageType=datasets&name=Alloy_DPA_v1_0&id=147)

## SemiCond

**model-branch** : `Domains_SemiCond`

* This dataset encompasses 20 semiconductors spanning from group IIB to VIA, namely Si, Ge, SiC, BAs, BN, AlN, AlP, AlAs, InP, InAs, InSb, GaN, GaP, GaAs, CdTe, InTe-In2Te3, CdSe-CdSe2, InSe-In2Se3, ZnS, CdS-CdS2. The configurations are explored by the DPGEN scheme in a temperature range of approximately 50.0 K to ∼4000 K and a pressure range of circa 1 bar to 50000 bar. DFT calculations, employed during the DP-GEN process, are computed utilizing the ABACUS software package. The energy cutoff of the DFT calculations was set to 100 Ry (∼ 1361 eV) and the mesh grid for K-space sampling was 0.08 Bohr−1 (∼ 0.15 Å−1).

## Cathode

**model-branch** : `Domains_Anode`

* This dataset explores O3-type layered oxide cathodes employed in lithium-ion and sodium- ion batteries. It has been generated utilizing the DP-GEN scheme. Specifically, the systems analyzed include LixTMO2 and NaxTMO2, where TM represents transition metal elements including Ni, Mn, Fe, Co, and Cr. The configuration space is explored by NPT MD simula- tions in a wide range of temperatures and pressures, varying from 50.0 K to 1250.0 K and 0 bar to 3000 bar, respectively. The DFT calculations for this dataset were conducted using the VASP software, incorporating the PBE-GGA functional. The dataset comprises su-percells containing twelve formula units for various systems, including LiTMO2, NaTMO2, Li0.5TMO2, Na0.5TMO2, and TMO2.

## Cluster

**model-branch** : `Domains_Cluster`

* This dataset is composed of metal nano-clusters. The dataset is decomposed in a non- overlapping way into two subsets, Cluster-P and Cluster-D, which are used for pre-training and downstream tasks, respectively. The Cluster-P dataset encompasses 9 types of clusters that are composed of one element, namely Au, Ag, Al, Cu, Ni, Pt, Pd, Si, and Ru, and 15 types of clusters composed of a combination of 2 elements, namely AgCu, AgNi, AgPd, AgPt, AuAg, AuCu, AuNi, AuPd, AuPt, CuNi, CuPd, CuPt, NiPd, PtNi, and PtPd. The Cluster-D dataset includes 7 types of clusters composed of ternary combination of metal elements, i.e. AgCuPt, AuAgCu, AuAgPd, AuAgPt, AuCuPd, AuCuPt, and PtPdNi. The configurations of the clusters in the Cluster-P and Cluster-D datasets are explored using the DPGEN scheme. The DFT calculations are performed using CP2K with PBE exchange-correlation functional and Grimme D3 dispersion correction.

## Drug

**model-branch**: `Domains_Drug`

* This dataset is generated using the DP-GEN approach and encompasses an extensive col- lection of over 1.4 million structures, comprising 8 elements H, C, N, O, F, Cl, S, and P, with the inclusion of up to 70 heavy atoms. The foundation for the initial training data was established by optimizing small molecules procured from the ChEMBL database with the aid of Gaussian software. To expand the dataset, high-temperature simulations were employed, and the data pool was further augmented by optimizing larger molecules from the ChEMBL database, followed by conducting supplementary simulations. In addition, unoptimized structures were randomly selected and subjected to simulations, resulting in the enlargement of the training set to encompass over 1 million conformations. To ensure comprehensive torsion coverage, structures originating from the ChEMBL torsion scans dataset were optimized and simulated, while enhanced sampling MD simulations performed with molecules contributed additional structures to the dataset.

## FerroEle

**model-branch** : `Domains_FerroEle`

* This dataset comprises 26 ABO3-type perovskite oxides, which span an extensive composition space containing elements such as Pb, Sr, Ba, Ca, Bi, K, Na, and their various combinations for the A-site, in addition to Ti, Nb, Zr, Mg, Zn, In, Hf, and their respective combinations for the B-site. The configurations of the materials were generated utilizing the DPGEN method and subsequently employed for the training of a universal interatomic potential for perovskite oxides, referred to as UniPero. All DFT calculations were executed with the aid of the ABACUS software, utilizing the PBEsol functional within the GGA framework and ONCV multi-projector pseudopotentials. This dataset is divided into four distinct segments according to data complexity.

## OC2M

**model-branch** : `Domains_OC2M`

* This dataset constitutes a subset derived from the Open Catalyst Project’s comprehensive OC20 dataset, which is inclusive of approximately 2 million DFT data samples. This particular dataset comprises 56 distinct elements, with the samples depicting DFT relaxations associated with molecular adsorptions on various surfaces, spanning an extensive structure and chemical space. The principal focus of these samples is directed toward 82 adsorbates that hold significance in the context of renewable energy production and environmental applications.

## SSE-PBE

**model-branch** : `Domains_SSE-PBE`

* This dataset comprises solid-state electrolytes generated through the DP-GEN method. It is composed of three distinct chemical formulas, namely Li10GeP2S12, Li10SiP2S12, and Li10SnP2S12. All DFT calculations were conducted employing the VASP software, with the application of PBE exchange-correlation functional.

## H2O-PD

**model-branch** : `H2O_H2O-PD`

* The water/ice dataset is used to train a DP model for the calculation of the phase diagram of water in the thermodynamic range of 0 to 2400 K and 0 to 50 GPa. The dataset was labeled by the VASP software with the SCAN exchange-correlation functional. The energy cutoff was set to 1500 eV and the spacing of the K-space lattice was 0.5 Å−1.

## AgAu-PBE

**model-branch** : `Metals_AgAu-PBE`

* This dataset contains Ag, Au and AgAu configurations that were generated by the DP-GEN scheme. DFT calculations were conducted employing VASP software, in conjunction with PBE functional.

## AlMgCu

**model-branch** : `Metals_AlMgCu`

* This dataset contains unitary, binary, and ternary alloys of Al, Cu, and Mg, i.e. AlxCuyMgz with a concentration range of 0 ≤ x,y,z ≤ 1,x + y + z = 1. The configurations are explored by the DP-GEN scheme in a temperature range of 50.0 K to 2579.8 K and a pressure range of 1 to 50,000 bar (5 GPa). Energy, force, and virial labels are obtained by DFT calculations adopting the PBE fucntional using VASP. The energy cut-off for PAW basis sets is 650 eV. K-points are sampled by Monkhorst-Pack mesh with a grid spacing of 0.1 Å−1. Order 1 Methfessel-Paxton smearing is used with σ = 0.22 eV. SCF convergence criterion for DFT calculation is 1 × 1e−6 eV.

## ANI-1x

**model-branch** : `Domains_ANI`

* This dataset is generated through iterative active learning to efficiently sample chemical space relevant for machine-learned potentials. It contains over 5 million conformations of organic molecules with up to 13 heavy atoms. Initial data came from small GDB-11 molecules. More complex chemical space was then explored by conformational sampling of progressively larger molecules from databases like ChEMBL and GDB-13. Techniques included diverse normal mode sampling, trajectory sampling, and dimer sampling to capture molecular diversity. By combining automated active learning of molecules and conforma- tions with rigorous sampling methods, ANI-1x provides comprehensive coverage of organic chemical space.

## Transition-1x

**model-branch** : `Domains_Transition1x`

* The dataset comprises over 9.6 million conformations of organic small molecules, spanning more than 10,000 distinct organic chemical reactions. Each reaction involves up to seven heavy atoms, including carbon, nitrogen, and oxygen. Originating from the GDB-7 database, the dataset selects structures that serve as reactants; potential products are then generated via the Growing String Method. Reaction trajectories for these reactant-product pairs are computed employing the Nudged Elastic Band (NEB) method. By selectively sampling structures produced during the NEB procedure and discarding non-physical conformations, the dataset effectively encapsulates the chemical space pertinent to reaction pathways.

## MPtraj

**model-branch** : `MP_traj_v024_alldata_mixu`

* The Materials Project Trajectory dataset. See [here](https://figshare.com/articles/dataset/Materials_Project_Trjectory_MPtrj_Dataset/23713842?file=41619375) for more information.

## Other datasets

Additional datasets include Cu, Sn, Ti, V, W, C12H26, HfO2, In2Se3 H2O-DPLR, H2O-SCAN0, H2O-PBE0TS, and H2O-PBE0TS-MD.

# References

If you use this model, please cite our paper:

```markdown
@misc{zhang2024dpa2largeatomicmodel,
      title={DPA-2: a large atomic model as a multi-task learner}, 
      author={Duo Zhang and Xinzijian Liu and Xiangyu Zhang and Chengqian Zhang and Chun Cai and Hangrui Bi and Yiming Du and Xuejian Qin and Anyang Peng and Jiameng Huang and Bowen Li and Yifan Shan and Jinzhe Zeng and Yuzhi Zhang and Siyuan Liu and Yifan Li and Junhan Chang and Xinyan Wang and Shuo Zhou and Jianchuan Liu and Xiaoshan Luo and Zhenyu Wang and Wanrun Jiang and Jing Wu and Yudi Yang and Jiyuan Yang and Manyi Yang and Fu-Qiang Gong and Linshuang Zhang and Mengchao Shi and Fu-Zhi Dai and Darrin M. York and Shi Liu and Tong Zhu and Zhicheng Zhong and Jian Lv and Jun Cheng and Weile Jia and Mohan Chen and Guolin Ke and Weinan E and Linfeng Zhang and Han Wang},
      year={2024},
      eprint={2312.15492},
      archivePrefix={arXiv},
      primaryClass={physics.chem-ph},
      url={https://arxiv.org/abs/2312.15492}, 
}
```